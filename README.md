# NGINX + PHP
**Add Environment Variable**

   | Key             | Value           |
   | --------------- | --------------- |
   | `DATABASE_URL`  | |
   | `DB_CONNECTION`  | |
   | `APP_KEY`  | `php artisan key:generate --show` |

# NGINX + PHP + PostgreSQL
**Add Environment Variable**

   | Key             | Value           |
   | --------------- | --------------- |
   | `DATABASE_URL`  | The **Internal Connection String** for the database you created above. |
   | `DB_CONNECTION`  | `pgsql` |
   | `APP_KEY`  | `php artisan key:generate --show` |
